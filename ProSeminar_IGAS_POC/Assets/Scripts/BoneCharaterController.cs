﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoneCharaterController : MonoBehaviour
{
    public MeshRenderer body;
    public MeshRenderer legtopleft;
    public MeshRenderer legtopright;
    public MeshRenderer legbottomleft;
    public MeshRenderer legbottomright;
    public MeshRenderer armlefttop;
    public MeshRenderer armleftbottom;
    public MeshRenderer armrighttop;
    public MeshRenderer armrightbottom;
    public MeshRenderer feedleft;
    public MeshRenderer feedright;

    public Color green;
    public Color red;
    public Color yellow;

    public int timesKniebeuge = 5;

    private int KniebeugenRemaining;
    // Use this for initialization
    void Start ()
    {
       
    }
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.E))
	    {
	        KniebeugenRemaining = timesKniebeuge;

            StartCoroutine(PlayAnimInterval());
        }
	}

    private IEnumerator PlayAnimInterval()
    {
        while (KniebeugenRemaining > 0)
        {
            GetComponent<Animator>().Play("Kniebeuge");
            KniebeugenRemaining--;
            yield return new WaitForSeconds(4);
        }
    }

    private void turnArmColorToRed()
    {
        setColor(armleftbottom, red);
        setColor(armlefttop, red);
        setColor(armrightbottom, red);
        setColor(armrighttop, red);
    }

    private void turnArmColorToGreen()
    {
        setColor(armleftbottom, green);
        setColor(armlefttop, green);
        setColor(armrightbottom, green);
        setColor(armrighttop, green);
    }

    private void turnLegsToGreen()
    {
        setColor(legbottomleft, green);
        setColor(legbottomright, green);
        setColor(legtopleft, green);
        setColor(legtopright, green);
    }
    private void turnLegsToYellow()
    {
        setColor(legbottomleft, yellow);
        setColor(legbottomright, yellow);
        setColor(legtopleft, yellow);
        setColor(legtopright, yellow);
    }
    private void turnLegsToRed()
    {
        setColor(legbottomleft, red);
        setColor(legbottomright, red);
        setColor(legtopleft, red);
        setColor(legtopright, red);
    }

    private void turnBodyToGreen()
    {
        setColor(body, green);
    }
    private void turnBodyToRed()
    {
        setColor(body, red);
    }

    public void turnFeedToGreen()
    {
        setColor(feedleft,green);
        setColor(feedright,green);
    }
    public void turnFeedToRed()
    {
        setColor(feedleft, red);
        setColor(feedright, red);
    }
    public void turnFeedToYellow()
    {
        setColor(feedleft, yellow);
        setColor(feedright, yellow);
    }
    private void clearColor()
    {
        setColor(body, Color.white);
        setColor(legtopleft, Color.white);
        setColor(legtopright, Color.white);
        setColor(legbottomleft, Color.white);
        setColor(legbottomright, Color.white);
        setColor(armlefttop, Color.white);
        setColor(armleftbottom, Color.white);
        setColor(armrighttop, Color.white);
        setColor(armrightbottom, Color.white);
        setColor(feedright, Color.white);
        setColor(feedleft, Color.white);
    }

    private void setColor(Renderer r, Color c)
    {
        r.material.shader = Shader.Find("_Color");
        r.material.SetColor("_Color", c);
        r.material.shader = Shader.Find("Specular");
        r.material.SetColor("_SpecColor", c);
    }

}
