﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class CameraCTRL : MonoBehaviour
{
    private Rigidbody rb;

    public float speed;

    public Animator CameraAnimator;
	// Use this for initialization
	void Start ()
	{
	    rb = GetComponent<Rigidbody>();
	    CameraAnimator = GetComponent<Animator>();
	    CameraAnimator.enabled = false;

	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetAxis("Mouse X")!=0)
	    {
	        transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0) * Time.deltaTime * speed);
        }
        if(Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A))
	    {
	        rb.AddForce(speed * transform.forward);
	        rb.AddForce(speed * transform.right*-1);
        }
	    else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D))
        {
            rb.AddForce(speed * transform.forward);
            rb.AddForce(speed * transform.right * 1);
        }
        else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A))
        {
            rb.AddForce(speed* transform.forward*-1);
            rb.AddForce(speed* transform.right*-1);
        }
        else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D))
        {
            rb.AddForce(speed* transform.forward*-1);
            rb.AddForce(speed* transform.right*-1);
        }
        else if (Input.GetKey(KeyCode.W))
	    {
            rb.AddForce(speed * transform.forward);
	    }
	    else if (Input.GetKey(KeyCode.A))
        {
            rb.AddForce(speed*transform.right*-1);
        }
	    else if (Input.GetKey(KeyCode.S))
	    {
	        rb.AddForce(speed*transform.forward * -1);
        }
	    else if (Input.GetKey(KeyCode.D))
	    {
	        rb.AddForce(speed*transform.right);
        }
	    else
	    {
           rb.velocity=Vector3.zero;
	    }
	    if (Input.GetKeyDown(KeyCode.F))
	    {
	        CameraAnimator.enabled = true;

	        CameraAnimator.Play("camera");
        }
    }
}
