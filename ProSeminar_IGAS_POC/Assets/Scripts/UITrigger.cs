﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITrigger : MonoBehaviour
{
    public UIGlobal UIData;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void incrementCurÜbung()
    {
        UIData.currentÜbungen++;
    }

    void setMaxÜbungen(int pMax)
    {
        UIData.maxÜbungen = pMax;
    }

    void resetPercentage()
    {
        UIData.Übungspercentage = 0f;
    }

    void incPercentage()
    {
        UIData.Übungspercentage += 0.125f;
    }
}
