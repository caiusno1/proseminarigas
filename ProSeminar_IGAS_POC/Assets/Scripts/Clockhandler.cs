﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clockhandler : MonoBehaviour
{

    public UIGlobal uidata;

    public Transform zeigerTransform;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    zeigerTransform.eulerAngles=new Vector3(0,0,360-360*uidata.Übungspercentage);
	}
}
