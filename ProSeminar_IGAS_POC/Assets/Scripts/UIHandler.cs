﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{
    public UIGlobal globalUIData;

    public Text ÜbungnameText;

    public Text Übungscount;

    public BoneCharaterController[] characters;
	// Use this for initialization
	void Start ()
	{
	    globalUIData.currentÜbungen = 0;
	    globalUIData.Übungspercentage = 0;
	    foreach (BoneCharaterController character in characters)
	    {
	        character.timesKniebeuge = globalUIData.maxÜbungen;
	    }
	}
	
	// Update is called once per frame
	void Update ()
	{
	    ÜbungnameText.text = "Übung: "+globalUIData.Übungsname;
	    Übungscount.text = globalUIData.currentÜbungen + "/" + globalUIData.maxÜbungen;
	    if (Input.GetKeyDown(KeyCode.E))
	    {
	        globalUIData.currentÜbungen = 0;
	        globalUIData.Übungspercentage = 0f;
	    }

	}
}
