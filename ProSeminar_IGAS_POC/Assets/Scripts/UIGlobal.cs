﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UIData", menuName = "Global/UIData", order = 1)]
public class UIGlobal : ScriptableObject
{
    public string Übungsname;
    public int maxÜbungen;
    public int currentÜbungen;
    [Range(0,1)]
    public float Übungspercentage;
}
